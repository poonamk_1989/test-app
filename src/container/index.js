import React, { Component } from "react";
import Select from "../components/Select";
import { data } from "../components/options";
import { Container, Row } from "reactstrap";

const counter = 0;
class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectArr: [],
    };
  }

  componentDidMount() {
    this.onSelect(data);
  }

  // call the function when value is selected from the component and identify the next action
  onSelect = (args, val = "", identityKey = counter) => {
    let tmpArr = [...this.state.selectArr];
    const isDuplicate = this.checkDuplicate(tmpArr, val, identityKey, args);
    if (isDuplicate) {
      return;
    }

    if (args && args.length) {
      const arr = this.onRemove(tmpArr, identityKey);
      const elem = [
        <Select
          options={args}
          onSelect={this.onSelect}
          identityKey={identityKey}
          selected_val={val}
          key={identityKey}
        />,
      ];
      this.setState({
        selectArr: [...arr, ...elem],
      });
    } else {
      this.setState({
        selectArr: this.onRemove(tmpArr, identityKey),
      });
    }
  };

  // remove element from Remove all elements after index
  onRemove = (arr, index) => {
    arr.splice(index);
    return arr;
  };

  checkDuplicate = (arr, selectedVal, index) => {
    const found = arr.find(
      (element) =>
        element.props.identityKey === index &&
        element.props.selectedVal === selectedVal
    );
    return found ? true : false;
  };

  render() {
    const { selectArr } = this.state;
    return (
      <Container>
        <Row xs="1" sm="2" md="5">
          {selectArr.map((row) => {
            return row;
          })}
        </Row>
      </Container>
    );
  }
}

index.propTypes = {};

export default index;
