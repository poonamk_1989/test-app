import React from "react";
import Select from "react-select";

const SelectInput = ({ options, onSelect, identityKey }) => {
  return (
    <Select
      options={options}
      key={identityKey}
      autoFocus={true}
      onChange={(val) => onSelect(val.data, val.value, identityKey + 1)}
    />
  );
};

export default SelectInput;
