export const data = [
  {
    value: "firstLevel1",
    label: "firstLevel1",
    data: [
      {
        value: "secondLevel1",
        label: "secondLevel1",
        data: [
          {
            value: "thirdLevel1",
            label: "thirdLevel1",
            data: [
              {
                value: "fourthLevel1",
                label: "fourthLevel1",
                data: [
                  {
                    value: "fifthLevel1",
                    label: "fifthLevel1",
                  },
                ],
              },
            ],
          },
        ],
      },
      {
        value: "secondLevel2",
        label: "secondLevel2",
        data: [
          {
            value: "thirdLevel2",
            label: "thirdLevel2",
          },
        ],
      },
    ],
  },
  {
    value: "firstLevel2",
    label: "firstLevel2",
    data: [
      {
        value: "secondLevel2",
        label: "secondLevel2",
      },
    ],
  },
  {
    value: "firstLevel3",
    label: "firstLevel3",
    data: [],
  },
];
